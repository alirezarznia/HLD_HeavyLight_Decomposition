//#include <GOD>
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>
#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<ll,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
typedef long long ll;
using namespace std;
 ll n  ;
 vector<piii>vec[10001];
 ll sz[10001];
 ll chn[10001];
 ll hchn[10001];
 ll arr[10001];
 ll loc[10001];
 ll d[10001];
 ll seg[70000];
 ll pr[10001][16];
 ll deeper[10001];
 ll DFS(ll u , ll p, ll dep){
    pr[u][0]=p;
    d[u]=dep;
    for(auto i : vec[u]){
        if(i.second.first != p){ DFS(i.second.first, u ,dep+1);
            sz[u]+=sz[i.second.first];
        }
    }
    sz[u]++;
    return 0;
}
ll cnt=0 , ch=0;
ll HLD(ll u , ll p){
    ll hevnum=0 , wat=0;
    chn[u]=ch;
    if(hchn[ch]==-1) hchn[ch]=u;
    ll mx= -1 ,nmx=0  ,cc=0;
    for(auto i :vec[u]){
        if(i.second.first != p && mx <sz[i.second.first])
            mx=sz[i.second.first] ,nmx=i.second.first,hevnum=i.first,wat=cc;
            cc++;
    }
    if(mx != -1){
        arr[++cnt]=vec[u][wat].second.second ,loc[nmx]=cnt;
        deeper[hevnum]=nmx;
        HLD(nmx , u);
        for(auto i :vec[u])
            if(i.second.first != p && i.second.first != nmx)
                ch++,arr[++cnt]=i.second.second ,loc[i.second.first]=cnt ,deeper[i.first]=i.second.first,HLD(i.second.first ,u);
    }
    return 0;
}
ll COT(ll l , ll h , ll num){
    if(l==h) return seg[num] = arr[l];
    ll mid= (l+h)/2;
    return seg[num]= max(COT(l , mid , 2*num+1) ,COT(mid+1 ,h ,2*num+2));
}
ll SS(ll l , ll h , ll num ,ll ss ,ll ee){
        if(l>ee || h<ss) return -1;
       if(l>=ss && h<=ee) return seg[num];
    ll mid= (l+h)/2;
    return max(SS(l , mid , 2*num+1,ss,ee) ,SS(mid+1 ,h ,2*num+2,ss,ee));
}
ll LCA(){
    For(i ,1, 15){
        For(j ,1, n+1 ){
            if(pr[j][i-1] != -1) pr[j][i] =pr[pr[j][i-1]][i-1];
        }
    }
}

ll GLC(ll a ,ll b){
    if(d[a]<d[b]) swap(a,b);
    ll diff= d[a]-d[b];
    Rep(i, 15){
        if(diff&(1<<i)) a= pr[a][i];
    }
    if(a==b) return a;
    for(int i =15 ;i>=0;i--){
        if(pr[a][i]==pr[b][i]) continue;
        a=pr[a][i] ,b=pr[b][i];
    }
    return pr[a][0];
}
ll GET(ll a ,ll b){
    ll lc =GLC(a, b);
    ll mx = 0;
    while(1){
        if(chn[lc]==chn[a]){
            if(a!=lc)
                mx =max(mx, SS(0,n-1 , 0 ,loc[lc]+1 ,loc[a]));
            break;
        }
        else{
            mx=max(mx , SS(0 ,n-1 ,0 ,loc[hchn[chn[a]]] ,loc[a]));
            a=hchn[chn[a]], a=pr[a][0];
        }
    }
        while(1){
        if(chn[lc]==chn[b]){
            if(b!=lc)
                mx =max(mx, SS(0,n-1 , 0 ,loc[lc]+1 ,loc[b]));
            break;
        }
        else{
            mx=max(mx , SS(0 ,n-1 ,0 ,loc[hchn[chn[b]]] ,loc[b]));
            b=hchn[chn[b]], b=pr[b][0];
        }
    }
    return mx;
}
ll CHANGE(ll l ,ll h , ll num ,ll x ,ll y){
        if(l>x || h<x) return -1;
    if(l==h) return seg[num]=y;
    ll mid = (l+h)/2;
    return seg[num]= max(CHANGE(l ,mid ,2*num+1 ,x,y), CHANGE(mid+1, h ,2*num+2,x,y));
}
int main(){
  //  Test;
    ll t ;scanf("%lld" , &t);
    while(t--){
        arr[0]=-1;cnt=0;
        loc[1]=0;
        Set(hchn,-1);
        Set(pr,-1);
        scanf("%lld" , &n);
        Rep(i ,n+1) vec[i].clear();
        Rep(i ,n-1){
            ll u , v ,z ;scanf("%lld%lld%lld" , &u ,&v ,&z);
            vec[u].push_back(MP(i+1,MP(v,z)));
            vec[v].push_back(MP(i+1 , MP(u,z)));
        }
        DFS(1 ,-1 ,0);
        ch=0;
        HLD(1  , -1);
        COT(0 , n-1 , 0);
        LCA();
        while(1){
            ll a, b ; string str;
            cin>>str;
            if(str=="QUERY"){
                scanf("%lld%lld" , &a ,&b);
                printf("%lld\n" , GET(a,b));
            }
            if((str=="CHANGE")) {
                scanf("%lld%lld" , &a ,&b);
                CHANGE(0 ,n ,0 , loc[deeper[a]] , b);
            }
            if(str=="DONE")
            break;
        }
    }
    return 0;
}
